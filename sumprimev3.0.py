def sum_prime(lst):
    sums = 0
    flag = 1
    sub = iter(lst)
    try:
        while True:
            x = next(sub)
            if type(x) == list:
                plus = sum_prime(x)
                sums += plus
            else:
                num = x
                subs = iter(range(2,num))
                try:
                    while True:
                        y = next(subs)
                        if num % y == 0:
                            flag = 0
                            break
                        else:
                            flag = 1
                except StopIteration:
                    pass
                if num == 1:
                    flag = 0
                if num == 2:
                    flag = 1
                if flag == 1:
                    sums += num
    except StopIteration:
        pass
    return sums
print(sum_prime([2,3,[3,4,5]]))
